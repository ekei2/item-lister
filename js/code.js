var form = document.getElementById('addForm');
var itemList = document.getElementById('items');
var filter = document.getElementById('filter');
var itemsList = []

//  Init Pikaday
var picker = new Pikaday({
  field: document.getElementById('datepicker'),
  minDate: new Date(),
  maxDate: new Date(2020, 06, 31),
  format: 'DD MM YYYY'
});
// Init pikaday for edit page
var picker2 = new Pikaday({
  field: document.getElementById('datepicker-edit'),
  minDate: new Date(),
  maxDate: new Date(2020, 06, 31),
  format: 'DD MM YYYY'
});



// initialize the Table
setUptable();
// Form submit event
form.addEventListener('submit', addItem);

// listener for the filter search bar 
filter.addEventListener('keyup', filterItems);

// New addItems to input items into the Table rows

function addItem(e) {
  e.preventDefault();

  // Collecting the form data

  let itemName = document.querySelector('#item')
  let dueDate = document.querySelector('#datepicker')
  let item = new Item(itemName.value, dueDate.value);

  // Adding the form data to a List

  itemsList.push(item);
  addToStorage(itemsList);
  setUptable();
  clearInputs('#item', '#datepicker')
}

function addToStorage(itemArray) {
  localStorage.setItem('items', JSON.stringify(itemArray));
}

function retrieveFromStorage() {
  return localStorage.getItem('items');
}

function setUptable() {

  // retrieve the Data from local storage
  if (localStorage.getItem('items')) {
    itemsList = JSON.parse(localStorage.getItem('items'));
  }
  emptyTable()
  if (localStorage.getItem('items')) {
    let i = 0;
    itemsList.forEach(function (elem) {
      let tableContent = document.querySelector('#table-content')
      let row = tableContent.insertRow(-1)
      row.id = elem.id;
      let index = row.insertCell(0)
      let name = row.insertCell(1);
      let createDate = row.insertCell(2);
      let dueDate = row.insertCell(3)
      let status = row.insertCell(4);
      index.innerHTML = i + 1
      name.innerHTML = elem.name
      createDate.innerHTML = elem.stamp
      dueDate.innerHTML = elem.dueDate
      if (urgencyTester(elem.stamp, elem.dueDate) <= 2) {
        status.innerHTML = `<p class= 'urgent-text' >Urgent</p> <span><i class="fas fa-square urgent"></i></span> 
        <div class='e-d-wrapper'>
        <i class="far fa-trash-alt" ></i> <i class="fas fa-edit" data-toggle="modal" data-target="#edit-modal" ></i>
        </div>
        `
      } else if (urgencyTester(elem.stamp, elem.dueDate) >= 3 && urgencyTester(elem.stamp, elem.dueDate) <= 5) {
        status.innerHTML = `<p class= 'urgent-text' >Coming Up</p> <span><i class="fas fa-square moderate"></i></span>
        <div class='e-d-wrapper'>
        <i class="far fa-trash-alt"></i> <i class="fas fa-edit" data-toggle="modal" data-target="#edit-modal" ></i>
        </div>
        `
      } else {
        status.innerHTML = `<p class= 'urgent-text' >Relax </p> <span><i class="fas fa-square good"></i></span>
        <div class='e-d-wrapper'>
        <i class="far fa-trash-alt"></i> <i class="fas fa-edit" data-toggle="modal" data-target="#edit-modal" ></i>
        </div>
        `

      }
      //increment the table row counter
      i++
    })

    setDeleteListeners();

  }

}

function emptyTable() {
  const myTable = document.getElementById("table-content");
  while (myTable.firstChild) {
    myTable.removeChild(myTable.firstChild);
  }
}


// Remove item
function removeItem(e) {
  //get the element ID
  let row = e.currentTarget.parentNode.parentNode.parentNode
  itemsList.forEach(function (elem, index) {
    if (elem.id === row.id) {
      itemsList.splice(index, 1);
    }
  })

  row.parentNode.removeChild(row)
  addToStorage(itemsList)
  setUptable();
}


// Filter Items
function filterItems(e) {
  // convert text to lowercase
  var text = e.target.value.toLowerCase();
  // Get lis
  let items = document.querySelectorAll('#table-content tr');
  console.log(Array.from(items))
  // Convert to an array
  Array.from(items).forEach(function (item) {
    var itemName = item.firstChild.nextSibling.innerHTML;
    //console.log('ddd', itemName);
    if (itemName.toLowerCase().indexOf(text) != -1) {
      console.log(item)
      item.style.position = '';
      item.style.top = ''
      item.style.left = '';

    } else {
      item.style.position = 'absolute';
      item.style.top = '-9999px'
      item.style.left = '9999px';

    }
  });
}

// Do validation here

function formValidation() {

}


// Check the Urgency of the task

function urgencyTester(createDate, dueDate) {
  dateCurrent = new Date();
  dateDue = new Date(dueDate);
  milliDiff = dateDue.getTime() - dateCurrent.getTime();
  return milliDiff / (1000 * 60 * 60 * 24)

}

function clearInputs(...inputs) {

  inputs.forEach(function (selector) {
    elem = document.querySelector(selector)
    elem.value = '';
  })
}

function setDeleteListeners() {
  let trashList = document.querySelectorAll('.fa-trash-alt')
  for (let i = 0; i < trashList.length; i++) {
    trashList[i].addEventListener('click', removeItem);
  }
}

function setEditListeners() {
  let addList = document.querySelectorAll('.fa-edit')
  for (let i = 0; i < addList.length; i++) {
    addList[i].addEventListener('click', setupEditModal);
  }
}

// Modal related code. I had to use Jquery here because bootstrap works with Jquery
// But another way would be to make your own modal. 
// Since i know JQuery i simply prefered to use the available resources.

let modal = $('#edit-modal').on('shown.bs.modal', function (e) {
  let row = e.relatedTarget.parentNode.parentNode.parentNode
  //set the ID attribute
  let att = document.createAttribute('data-user-ID');

  att.value = row.id
  document.getElementById('edit-modal').setAttributeNode(att)
  itemsList.forEach(function (elem, index) {
    if (elem.id === row.id) {
      //get model inputs
      let itemEdit = document.querySelector('#item-edit')
      let datepickerEdit = document.querySelector('#datepicker-edit')
      itemEdit.value = elem.name
      datepickerEdit.value = elem.dueDate
    }
  })
})

function editItem(e) {
  let itemEdit = document.querySelector('#item-edit')
  let datepickerEdit = document.querySelector('#datepicker-edit')
  itemsList.forEach(function (elem, index) {
    if (elem.id == document.querySelector('#edit-modal').getAttribute('data-user-id')) {
      itemsList[index].name = itemEdit.value
      itemsList[index].dueDate = datepickerEdit.value
      console.log(itemsList[index])
      addToStorage(itemsList);
      setUptable()
      $('#edit-modal').modal('hide')

    }
  })
}