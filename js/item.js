class Item{
    constructor(name, dueDate){
        this.id = this.createID();
        this.name = name;
        this.dueDate = dueDate
        this.stamp = this.createTimeStamp();
    }

    createTimeStamp(){
        this.stamp = new Date();
        console.log(`${new Date(this.stamp.toDateString())}`)
        // return `${this.stamp.getDay()} - ${this.stamp.getMonth()} - ${this.stamp.getFullYear()} ${this.stamp.getHours()} : ${this.stamp.getMinutes()} : ${this.stamp.getSeconds()}`
        return `${this.stamp.toDateString()}`
    }

    createID(){
        return '_' + Math.random().toString(36).substr(2, 9);
    }
}

